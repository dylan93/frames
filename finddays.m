function [days] = finddays(year,month,day,hr,min,sec)
%%% Find the fractional day throughout the year
%
% Functionally equivalent to algorithm in:
%
%   Fundamentals of Astrodynamics & Applications, David Vallado, Fourth Edition, 2013.
%
%   http://celestrak.com/software/vallado-sw.asp
%
%
% Author: Dylan Thomas
% Date: Feb 2, 2018
% Copyright: TBD
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Code functionality
%
% Inputs:
%       
%       year    - [years]   - Epoch year associated with this state
%       month   - [months]  - Month of the year
%       day     - [days]    - Day of the month
%       hr      - [hours]   - Current hour of the day (24hr)
%       min     - [minutes] - Minutes in the current hour
%       sec     - [seconds] - Seconds in the current minute
%
%
% Outputs:
%
%       days    - [days]    - Fractional days since beginning of year
%
%
% Dependencies:
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
% Saved days in month list. Check for leap years.
%
lmonth = [31 28 31 30 31 30 31 31 30 31 30 31];
if (rem(year,4) == 0)
    lmonth(2) = 29;
    if (rem(year,100) == 0) && (rem(year,400) ~= 0)
        lmonth(2) = 28;
    end
end

%
% Add up days in all the months past
ii    = 1;
days = 0.0;
while (ii < month) && (ii < 12)
    days = days + lmonth(ii);
    ii= ii + 1;
end

%
% Add day of month, days in past months, and fractional part up
% 
days = days + day + hr/24.0 + min/1440.0 + sec/86400.0;
end

