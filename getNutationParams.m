function [delPsi,trueEps,meanEps,Eq_e,Del] = getNutationParams(TTT,iar80,rar80,terms)
% Calculate Nutation Parameters - IAU-76 Reduction
%
% Determine the needed nutation parameters to successully transform between
% 'interial' frames using the theory from the IAU-76 Reduction. These 
% equations and constants are heavily derived from David Vallado's
% original code for his book and website:
%
%   Fundamentals of Astrodynamics & Applications, David Vallado, Fourth Edition, 2013.
%
%   http://celestrak.com/software/vallado-sw.asp
%
% More specifically, this code follows the methodology outline in section 
% 3.7 of his book. Note that some major changes were made, and a few 
% features were added to improve functionality.
%
% Author: Dylan Thomas
% Date: January 25, 2018
% Copyright: TBD
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Code functionality
%
% Inputs:
%       
%       TTT     - [jul cent] - Terrestrial time in Julian centuries
%       iar80   -            - IAU-80 coefficients for nutation.
%       rar80   -            - IAU-80 coefficients for nutation.
%       terms   -            - Number of terms to use in calculating the
%                              equation of the equinoxes
%
%
% Outputs:
%
%       delPsi  - [rad]     - Nutation in longitude
%       trueEps - [rad]     - True obliquity of the ecliptic
%       meanEps - [rad]     - Mean obliquity of the ecliptic
%       Eq_e    - [rad]     - Eqaution of equinoxes
%       Del     - [rad]     - Delaunay parameters
%
%
% Dependencies:
%
%       iau80in
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
% Conversion and time constants
%
DEG2RAD = pi / 180;                 % degree  -> radian
AS2DEG  = (1 / 3600);               % arc sec -> degree
AS2RAD  = AS2DEG * DEG2RAD;         % arc sec -> radian
TTT2 = TTT*TTT;
TTT3 = TTT2*TTT;

%
% Find the mean obliquity of the ecliptic. Convert to btwn [0,2pi].
% Vallado 4th Ed. Eqn 3-81 (could use Eqn 3-68?)
%
meanEps = -46.8150 * TTT - 0.00059 * TTT2 + 0.001813 * TTT3 + 84381.448;
meanEps = rem(meanEps / 3600, 360) * DEG2RAD;

%
% Determine coefficients for IAU-80 nutation. Convert to btwn [0,2pi].
% Save Delaunay params for debugging. TODO: Turn off
% Vallado 4th Ed. Eqn 3-82 (From Errata)
r      = 360;
M_L    = 134.96298139 + (1325*r + 198.8673981)*TTT + 0.0086972*TTT2 + 1.78e-5*TTT3;
M_S    = 357.52772333 + (99*r   + 359.0503400)*TTT - 0.0001603*TTT2 - 3.3e-6*TTT3;
mu_M_L = 93.27191028  + (1342*r + 82.0175381)*TTT  - 0.0036825*TTT2 + 3.1e-6*TTT3;
D_S    = 297.85036306 + (1236*r + 307.1114800)*TTT - 0.0019142*TTT2 + 5.3e-6*TTT3;
OM_L   = 125.04452222 - (5*r    + 134.1362608)*TTT + 0.0020708*TTT2 + 2.2e-6*TTT3;

M_L    = rem(M_L,360)    * DEG2RAD;
M_S    = rem(M_S,360)    * DEG2RAD;
mu_M_L = rem(mu_M_L,360) * DEG2RAD;
D_S    = rem(D_S,360)    * DEG2RAD;
OM_L   = rem(OM_L,360)   * DEG2RAD;

Del.M_L = M_L;
Del.M_S = M_S;
Del.mu_M_L = mu_M_L;
Del.D_S = D_S;
Del.OM_L = OM_L;

%
% Initialize & calculate nutation/obliquity corrections. Convert to btwn [0,2pi].
% Vallado 4th Ed Eq 3-83
%
delPsi = 0;
delEps = 0;

tempval  = iar80(:,1)*M_L + iar80(:,2)*M_S + iar80(:,3)*mu_M_L ...
           + iar80(:,4)*D_S + iar80(:,5)*OM_L;
delPsi   = delPsi + dot((rar80(:,1) + rar80(:,2)*TTT), sin(tempval));
delEps   = delEps + dot((rar80(:,3) + rar80(:,4)*TTT), cos(tempval));

delPsi = rem(delPsi,2*pi);
delEps = rem(delEps,2*pi);

%
% Get true obliquity of the ecliptic
%
trueEps  = meanEps + delEps;

%
% Get JD_start. small disconnect with TTT instead of ut1
% (Before Feb 27, 1997 extra terms not used. 
% TODO: Check UT1 vs. TTT, figure out this.
jdTTT = TTT * 36525 + 2451545;

% 
% Determine the equation of equinoxes. TODO: Should the second conversion
% be there?
% 
Eq_e = delPsi * cos(meanEps);
if (jdTTT > 2450449.5) && (terms > 0)
    Eq_e = Eq_e + 0.00264*AS2RAD*sin(OM_L) + 0.000063*AS2RAD*sin(2.0*OM_L);
end

end

