function [r_j2000,v_j2000,TR] = teme2j2000(r_teme,v_teme,yr,ep_days,eopData,iar80,rar80)
%%% TEME to ECI (J2000) Transformation
%
% Transform position and velocity vectors from True Equator, Mean Equinox 
% frame to the Earth-Centered Inertial (J2000) frame. These equations,
% constants, and general algorithm are heavily derived from David Vallado's
% original code for his book and website:
%
%   Fundamentals of Astrodynamics & Applications, David Vallado, Fourth Edition, 2013.
%
%   http://celestrak.com/software/vallado-sw.asp
%
% More specifically, this code follows the methodology outline in section 
% 3.7 of his book. Note that some major changes were made, and a few 
% features were added to improve functionality.
%
% Author: Dylan Thomas
% Date: January 25, 2018
% Copyright: TBD
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Code functionality
%
% Inputs:
%       
%       r_teme  - [km]      - 3x1 position state vector in the TEME frame
%       v_teme  - [km/s]    - 3x1 velocity state vector in the TEME frame
%       yr      - [years]   - Epoch year associated with this state
%       ep_days - [days]    - Days since Jan 1 of the current year
%
%
% Outputs:
%
%       r_j2000 - [km]      - 3x1 position state vector in the ECI frame
%       v_j2000 - [km/s]    - 3x1 velocity state vector in the ECI frame
%       TR      -           - 3x3 transformation matrix from TEME -> ECI
%
%
% Dependencies:
%
%       days2mdh, getEOPs, utc2TT, hms2sec, sec2hms, jday,
%       getNutationParams
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
% Conversion Constants & input checking
%
DEG2RAD = pi / 180;                 % degree  -> radian
AS2DEG  = (1 / 3600);               % arc sec -> degree
AS2RAD  = AS2DEG * DEG2RAD;         % arc sec -> radian

r_teme = reshape(r_teme,[3,1]);
v_teme = reshape(v_teme,[3,1]);

%
% Convert year and epoch day to mdhms form. Time always in UTC.
% Read in EOPs for this day & get terrestrial time, in Julian centuries
%
[mo,day,hr,min,sec] = days2mdh(yr,ep_days);

[~,~,~,~,~,~,dAT] = getEOPs(eopData,yr,mo,day);

[~,TTT] = utc2TT(yr,mo,day,hr,min,sec,dAT);

%
% Get nutation parameters. We use all 106 terms, and the two extra terms in 
% the Equation of Equinoxes. IAU-76/FK5 Reduction.
%
% EOP corrections not added when converting to J2000 according to Vallado
% 
[delPsi,trueEps,meanEps,Eq_e,~] = getNutationParams(TTT,iar80,rar80,2);

%
% Get precession angles. Vallado 4th Ed. Eq 3-88
%
ze = (2306.2181*TTT + 0.30188*TTT^2 + 0.017998*TTT^3) * AS2RAD;
th = (2004.3109*TTT - 0.42665*TTT^2 - 0.041833*TTT^3) * AS2RAD;
z  = (2306.2181*TTT + 1.09468*TTT^2 + 0.018203*TTT^3) * AS2RAD;

%
% Get all the rotation matrices. Get final rotation matrix from TEME to
% ECI frame.
%
R_teme2tod = rot3mat(-Eq_e);
R_tod2mod  = rot1mat(-meanEps)*rot3mat(delPsi)*rot1mat(trueEps);
R_mod2eci  = rot3mat(ze)*rot2mat(-th)*rot3mat(z);
TR         = R_mod2eci*R_tod2mod*R_teme2tod;

%
% Perform transformation to J2000 frame.
%
r_j2000 = TR*r_teme;
v_j2000 = TR*v_teme;

end