# Frame Transformations - Matlab

Package to provide highly accurate frame transformations for the purpose of Astrodynamics.

### TODO List:
- Allow loadData() to load in local variables with a flag
- Add in more frame transformations
    - ICRF
    - RSW
    - PQW
- Move all data files to data/ directory
- Add in more test cases in verifications
- Convert to an object-oriented structure
- Add in options for 'fast' transformations
- Add in IAU 2000 convention transformations