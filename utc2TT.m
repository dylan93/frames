function [TT,TTT] = utc2TT(yr,mo,day,hr,min,sec,dAT)
% Convert UTC to Terrestrial Time
%
% Convert UTC time, in ymdhms form, into terrestrial time, in units of 
% seconds and Julian centuries. Functionally equivalent to algorithm in:
%
%   Fundamentals of Astrodynamics & Applications, David Vallado, Fourth Edition, 2013.
%
%   http://celestrak.com/software/vallado-sw.asp
%
%%%% Note: convtime function in Vallado SOFTWARE omits fractional part:
% TTT = (jd - 2451545.0)/36525;
%
% Author: Dylan Thomas
% Date: January 25, 2018
% Copyright: TBD
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Code functionality
%
% Inputs:
%       
%       yr      - [years]   - Current year
%       mo      - [months]  - Month of the year
%       day     - [days]    - Day of the month
%       hr      - [hours]   - Hours in the current day (UTC)
%       min     - [minutes] - Minutes in the current hour (UTC)
%       sec     - [seconds] - Seconds in the current minute (UTC)
%       dAT     - [seconds] - Change in atomic time
%
%
% Outputs:
%
%       TT      - [sec]      - Terrestrial time
%       TTT     - [Jul cent] - Terrestrial time in Julian Centuries
%
% Dependencies:
%
%       hms2sec, sec2hms, jday
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
% Get UTC in seconds. Convert to atomic time and then terrestrial time in
% seconds. Eq. 3-49 Vallado Ed. 4
%
utc = hms2sec(hr,min,sec);
tai = utc + dAT;
TT = tai + 32.184; % Difference between atomic time and terrestrial time

%
% Convert terrestrial time back to hms form
%
[hrtemp,mintemp,sectemp] = sec2hms(TT);

%
% Get Julian day. fractional part separate to preserve sig digs
%
[jd, jdfrac] = jday(yr,mo,day,hrtemp,mintemp,sectemp);

%
% Convert to Julian centuries since J2000 epoch
% Eq. 3-42 Vallado 4th Ed.
%
TTT = (jd + jdfrac - 2451545)/36525;

end