% -----------------------------------------------------------------------------
%
%                           function sec2hms
%
%  this function converts seconds from the beginning of the day into hours,
%    minutes and seconds.
%
%  author        : david vallado                  719-573-2600   25 jun 2002
%
%  revisions
%                -
%
%  inputs          description                    range / units
%    utsec       - seconds                        0.0 .. 86400.0
%
%  outputs       :
%    hr          - hours                          0 .. 24
%    min         - minutes                        0 .. 59
%    sec         - seconds                        0.0 .. 59.99
%
%  locals        :
%    temp        - temporary variable
%
%  coupling      :
%    none.
%
% [hr,min,sec] = sec2hms( utsec );
% -----------------------------------------------------------------------------

function [hr,min,sec] = sec2hms(t_sec)
% Convert time from seconds from beginning of the day to hms form.
%
% This is equivalent to the function in:
%
%   Fundamentals of Astrodynamics & Applications, David Vallado, Fourth Edition, 2013.
%
%   http://celestrak.com/software/vallado-sw.asp
%
%
% Author: Dylan Thomas
% January 25, 2018
% Copyright: TBD
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Code functionality
%
% Inputs:
%       
%       t_sec   - [sec]      - Seconds since the beginning of the day
%
%
% Outputs:
%
%       hr      - [hours]   - Hours in the current day
%       min     - [minutes] - Minutes in the current hour
%       sec     - [seconds] - Seconds in the current minute
%
%
% Dependencies:
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

temp  = t_sec / 3600;
hr    = fix(temp);
min   = fix((temp - hr) * 60);
sec   = (temp - hr - min/60) * 3600;

