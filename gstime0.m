function gst0 = gstime0(year)
%%% Find sidereal time since beginning of the year
%
% Functionally equivalent to algorithm in:
%
%   Fundamentals of Astrodynamics & Applications, David Vallado, Fourth Edition, 2013.
%
%   http://celestrak.com/software/vallado-sw.asp
%
%
% Author: Dylan Thomas
% Date: Feb 2, 2018
% Copyright: TBD
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Code functionality
%
% Inputs:
%       
%       year    - [years]   - Epoch year
%
%
% Outputs:
%
%       gst0    - [radians] - Sidereal time since begninning of the year
%
%
% Dependencies:
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
% Constant
%
twopi = 2.0*pi;

%
% Get Julian date, then put it in Julian centuries
%
jd = 367*year - floor((7*(year + floor(10/12)))*0.25) + floor(275/9) + 1721014.5;
tut1 = (jd - 2451545)/36525;

%
% Calculate sidereal time since year start
%
temp = - 6.2e-6*tut1*tut1*tut1 + 0.093104*tut1*tut1  ...
       + (876600*3600 + 8640184.812866)*tut1 + 67310.54841;
temp = temp / 240;
temp = temp * pi / 180;

%
% Check quadrants
%
temp = rem(temp,twopi);
if (temp < 0)
   temp = temp + twopi;
end

gst0 = temp;
end

