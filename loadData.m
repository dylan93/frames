function [eopData,iar80,rar80] = loadData(jd_init)
% Retrieve Earth Orientation Parameters from a data file
%
% Read a data file from Celestrak, and return the corresponding Earth 
% Orientation Parameters. These help correct the IAU-76/FK5 reduction for 
% proper frame transformations. They include data from Jan 1, 1962 until 
% roughly 180 in the future. This also reads in  the IAU 1980 theory
% coefficients for use in precession data.
%
% 
% Author: Dylan Thomas
% Date: February 14, 2018
% Copyright: TBD
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Code functionality
%
% Inputs:
%       
%       jd_init - Julian day of the starting scenario epoch
%
%
% Outputs:
%
%       eopData - All EOP data pulled from Celestrak
%       iar80   - IAU 1980 data
%
%
% Dependencies:
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
% Perform a check on the beginning date for retrieving EOP data
%
if jd_init < jday(1962,1,1,0,0,0)
    error('You have selected a Julian date that is before the tabulated EOP data.')
elseif jd_init > jday(2013,1,1,0,0,0)
    url = 'http://celestrak.com/SpaceData/eop20130101.txt';
else
    url = 'http://celestrak.com/SpaceData/eop19620101.txt';
end

%
% Set option for reading webpage, and read the webpage.
%
opts = weboptions('Timeout',20);
data = webread(url,opts);

%
% These are strings that separate data in the file. We want to extract the
% indices of these strings to properly parse the file
%
beginObsStr = 'BEGIN OBSERVED';
endObsStr = 'END OBSERVED';
beginPredStr = 'BEGIN PREDICTED';
endPredStr = 'END PREDICTED';

%
% Get the appropriate indices for where to begin and stop reading.
%
beginObsIdx = regexp(data,beginObsStr);
[~,beginObsIdxEnd] = textscan(data(beginObsIdx:end),'%s',2);
startReadObsIdx = beginObsIdx + beginObsIdxEnd;
endReadObsIdx = regexp(data,endObsStr);

beginPredIdx = regexp(data,beginPredStr);
[~,beginPredIdxEnd] = textscan(data(beginPredIdx:end),'%s',2);
startReadPredIdx = beginPredIdx + beginPredIdxEnd;
endReadPredIdx = regexp(data,endPredStr);

%
% Read in appropriate EOP data.
%
formatSpec = '%d %d %d %d %f %f %f %f %f %f %f %f %f';
eopDataObs = textscan(data(startReadObsIdx:endReadObsIdx),formatSpec);
eopDataPred = textscan(data(startReadPredIdx:endReadPredIdx),formatSpec);

%
% Concatenate the dat into one continuous variable
%
n = length(eopDataObs);
eopData = cell(1,n);
for ii = 1:n
    eopData{ii} = [eopDataObs{ii}; eopDataPred{ii}];
end

convrt= 0.0001 * pi / (180*3600.0); % 0.0001" to radians
%
% Load coefficients for IAU 1980 data. Save them to variables.
%
nut80 = load('nut80.dat');
iar80 = nut80(:,1:5);
rar80 = nut80(:,6:9);
%
% Perform conversion
%
rar80 = rar80 * convrt;

end