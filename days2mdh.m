function [mon,day,hr,min,sec] = days2mdh(yr,ep_days)
%%% Convert Day of Year to MDHMS
%
% Convert day of the year in days into the quivalent month, day, hour,
% minute, and seconds. Functionally equivalent to algorithm in:
%
% Fundamentals of Astrodynamics & Applications, David Vallado, Fourth Edition, 2013.
%
% http://celestrak.com/software/vallado-sw.asp
%
% Author: Dylan Thomas
% Date: January 25, 2018
% Copyright: TBD
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Code functionality
%
% Inputs:
%       
%       yr      - [years]   - Epoch year associated with this state
%       ep_days - [days]    - Days since Jan 1 of the current year
%
%
% Outputs:
%
%       mon     - [months]  - Month of the year
%       day     - [days]    - Day of the month
%       hr      - [hours]   - Current hour of the day (24hr)
%       min     - [minutes] - Minutes in the current hour
%       sec     - [seconds] - Seconds in the current minute
%
%
% Dependencies:
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
% Set up array of days in month, check leap year. Get integer day of year.
% 
months = [31 28 31 30 31 30 31 31 30 31 30 31];
if rem(yr - 1900,4) == 0
    months(2) = 29;
end
day_of_yr = floor(ep_days);

%
% Find month and day of month.
%
ii = 1;
inttemp = 0;
while (day_of_yr > inttemp + months(ii)) && (ii < 12)
    inttemp = inttemp + months(ii);
    ii = ii + 1;
end
mon = ii;
day = day_of_yr - inttemp;

%
% Find hours, minutes, and seconds.
%
hr_rem  = (ep_days - day_of_yr) * 24.0;
hr      = fix(hr_rem);
min_rem = (hr_rem - hr) * 60.0;
min     = fix(min_rem);
sec     = (min_rem - min) * 60.0;

end

