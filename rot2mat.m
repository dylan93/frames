function [rotmat] = rot2mat(xval)
%%% Perform a second axis rotation.
%
% Author: Dylan Thomas
% Date: Feb 2, 2018
% Copyright: TBD

c = cos(xval);
s = sin(xval);

rotmat = [c 0 -s; 0 1 0; s 0 c];
end