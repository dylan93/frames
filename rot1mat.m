function [rotmat] = rot1mat(xval)
%%% Perform a first axis rotation.
%
% Author: Dylan Thomas
% Date: Feb 2, 2018
% Copyright: TBD

c = cos(xval);
s = sin(xval);

rotmat = [1 0 0; 0 c s; 0 -s c];
end