%%% Script to test out coordinate transformations
%
% Currently this script tests the following transformations:
%
%   ECEF (ITRF) <-> J2000 (ECI)
%   ECEF (ITRF) <-> GCRF (ECI)
%
% Author: Dylan Thomas
% Date: Feb 2, 2018
% Copyright: TBD
%
%
clearvars; clc; close all;

%
% Load in saved data for testing/verification.
% 
load ex3_15.mat

%
% Get the MDH form of time. Convert to Julian Day and retrieve data for
% this test
%
[mon,day,hr,min,sec] = days2mdh(year,ep_days);
[jd,jdfrac] = jday(year,mon,day,hr,min,sec);
[eopData,iar80,rar80] = loadData(jd+jdfrac);

%
% Test the ECEF to J2000 transformation & inverse
%
[r_j,v_j] = ecef2j2000(r_ecef,v_ecef,[0;0;0],year,ep_days,eopData,iar80,rar80);
fprintf('ECEF to J2000\n\n')
compare_vectors(r_j2000,v_j2000,r_j,v_j);

[r_ef,v_ef] = j2000_2ecef(r_j2000,v_j2000,[0;0;0],year,ep_days,eopData,iar80,rar80);
fprintf('-----------------------------------------------------\n')
fprintf('J2000 to ECEF\n\n')
compare_vectors(r_ecef,v_ecef,r_ef,v_ef);

%
% Test the ECEF to GCRF transformation & inverse
%
[r_gc,v_gc] = ecef2gcrf(r_ecef,v_ecef,[0;0;0],year,ep_days,eopData,iar80,rar80);
fprintf('-----------------------------------------------------\n')
fprintf('ECEF to GCRF\n\n')
compare_vectors(r_gcrf,v_gcrf,r_gc,v_gc);

[r_ef,v_ef] = gcrf2ecef(r_gcrf,v_gcrf,[0;0;0],year,ep_days,eopData,iar80,rar80);
fprintf('-----------------------------------------------------\n')
fprintf('GCRF to ECEF\n\n')
compare_vectors(r_ecef,v_ecef,r_ef,v_ef);

