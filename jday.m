function [jd,jdfrac] = jday(yr,mon,day,hr,min,sec)
% Convert Date & UTC to Julian date
%
% Convert UTC date & time, in ymdhms form, Julian days. The epoch measured 
% from is January 1, 4713 BC, 12:00 which is the common point for the solar
% cycle, Metonic cycle, and Roman Indication. This function separates the 
% fractional part to save significant digits.
%
% Functionally equivalent to algorithm in:
%
%   Fundamentals of Astrodynamics & Applications, David Vallado, Fourth Edition, 2013.
%
%   http://celestrak.com/software/vallado-sw.asp
%
%
% Author: Dylan Thomas
% Date: January 25, 2018
% Copyright: TBD
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Code functionality
%
% Inputs:
%       
%       yr      - [years]   - Current year
%       mo      - [months]  - Month of the year
%       day     - [days]    - Day of the month
%       hr      - [hours]   - Hours in the current day (UTC)
%       min     - [minutes] - Minutes in the current hour (UTC)
%       sec     - [seconds] - Seconds in the current minute (UTC)
%
%
% Outputs:
%
%       jd      - [days]     - Julian date integer value
%       jdfrac  - [days]     - Julian date decimal value
%
% Dependencies:
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
% Get integer portion of Julian day. This equation is easily changed to
% calculate the modified Julian date (mjd) by subtracting 2400000.5, so
% that the last term is (-678987) and not (1721013.5)
%
jd = 367 * yr - floor((7 * (yr + floor((mon + 9) / 12))) * 0.25) ...
     + floor(275 * mon / 9) + day + 1721013.5;
%
% Get the decimal portion of the Julian date separately to save significant
% digits
%
jdfrac = (sec + min * 60 + hr * 3600) / 86400;

%
% Perform a sanity check on the fractional part
%
if jdfrac > 1.0
    jd = jd + floor(jdfrac);
    jdfrac = jdfrac - floor(jdfrac);
end

end