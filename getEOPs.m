function [xp,yp,dut1,lod,dPsi,dEps,dAT] = getEOPs(eopData,yr,mo,day)
% Retrieve Earth Orientation Parameters from a data file
%
% Read a data file, and return the corresponding Earth Orientation Parameters.
% These help correct the IAU-76/FK5 reduction for proper frame
% transformations. They include data from Jan 1, 1962 -> roughly six months
% in the future.
%
% The EOPs are pulled from:
%
%   http://celestrak.com/SpaceData/
%
% 
% Author: Dylan Thomas
% Date: January 25, 2018
% Copyright: TBD
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Code functionality
%
% Inputs:
%       
%       eopData -           - {1x13} Cell of EOP data to index
%       yr      - [years]   - Year to lookup
%       mo      - [months]  - Month of the year to lookup
%       day     - [days]    - Day of the month to lookup
%
%
% Outputs:
%
%       xp
%       yp
%       dut1
%       lod
%       dPsi
%       dEps
%       dAT
%
%
% Dependencies:
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
% Perform sanity check to make sure the epoch is not past the last date.
%
if jday(yr,mo,day,0,0,0) > jday(eopData{1}(end),eopData{2}(end),eopData{3}(end),0,0,0)
    error('Input date exceeds the final date in EOP data.')
end

%
% Get the index associated with the right date, and extract the EOPs
%
idx = find((eopData{1} == yr & eopData{2} == mo) & eopData{3} == day);
xp = eopData{5}(idx);     yp = eopData{6}(idx);
dut1 = eopData{7}(idx);  lod = eopData{8}(idx);
dPsi = eopData{9}(idx); dEps = eopData{10}(idx);
dAT = eopData{13}(idx);

end