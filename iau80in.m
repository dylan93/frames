function [iar80,rar80] = iau80in()
%%% Load coefficients for iau80 convention.
%
% Derived from David Vallado's original book & code:
%
%   Fundamentals of Astrodynamics & Applications, David Vallado, Fourth Edition, 2013.
%
%   http://celestrak.com/software/vallado-sw.asp
%
%
% Author: Dylan Thomas
% Date: Feb 2, 2018
% Copyright: TBD
%
% 0.0001" to radians
%
convrt= 0.0001 * pi / (180*3600.0);
%
% Load coefficients. Save them to variables.
%
nut80 = load('nut80.dat');
iar80 = nut80(:,1:5);
rar80 = nut80(:,6:9);
%
% Perform conversion
%
rar80 = rar80 * convrt;
end
