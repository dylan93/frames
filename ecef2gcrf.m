function [r_gcrf,v_gcrf,a_gcrf] = ecef2gcrf(r_ecef,v_ecef,a_ecef,yr,ep_days,eopData,iar80,rar80)
%%% ECEF (ITRF) to ECI (GCRF) Transformation
%
% Transform position and velocity vectors from Earth-Centered, Earth-Fixed
% frame (ECEF) to the Earth-Centered Inertial (GCRF) frame. These equations,
% constants, and general algorithm are heavily derived from David Vallado's
% original code for his book and website:
%
%   Fundamentals of Astrodynamics & Applications, David Vallado, Fourth Edition, 2013.
%
%   http://celestrak.com/software/vallado-sw.asp
%
% More specifically, this code follows the methodology outline in section 
% 3.7 of his book. Note that some major changes were made, and a few 
% features were added to improve functionality.
%
% Author: Dylan Thomas
% Date: January 25, 2018
% Copyright: TBD
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Code functionality
%
% Inputs:
%       
%       r_ecef  - [km]      - 3x1 position state vector in the ECEF frame
%       v_ecef  - [km/s]    - 3x1 velocity state vector in the ECEF frame
%       a_ecef  - [km/s]    - 3x1 acceleration state vector in the ECEF frame
%       yr      - [years]   - Epoch year associated with this state
%       ep_days - [days]    - Days since Jan 1 of the current year
%
%
% Outputs:
%
%       r_gcrf  - [km]      - 3x1 position state vector in the ECI frame
%       v_gcrf  - [km/s]    - 3x1 velocity state vector in the ECI frame
%       a_gcrf  - [km/s]    - 3x1 acceleration state vector in the ECI frame
%
%
% Dependencies:
%
%       days2mdh, getEOPs, utc2TT, hms2sec, sec2hms, jday, rot3mat,
%       getNutationParams, findays, gstime0, rot1mat, rot2mat
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%
% Conversion Constants & input checking
%
DEG2RAD = pi / 180;                 % degree  -> radian
AS2DEG  = (1 / 3600);               % arc sec -> degree
AS2RAD  = AS2DEG * DEG2RAD;         % arc sec -> radian

r_ecef = reshape(r_ecef,[3,1]);
v_ecef = reshape(v_ecef,[3,1]);
a_ecef = reshape(a_ecef,[3,1]);

%
% Convert year and epoch day to mdhms form. Time always in UTC.
% Read in EOPs for this day & get terrestrial time, in Julian centuries
%
[mo,day,hr,min,sec] = days2mdh(yr,ep_days);

[xp,yp,dut1,lod,dDPsi,dDEps,dAT] = getEOPs(eopData,yr,mo,day);

[~,TTT] = utc2TT(yr,mo,day,hr,min,sec,dAT);

%
% Get nutation parameters. We use all 106 terms, and the two extra terms in 
% the Equation of Equinoxes. IAU-76/FK5 Reduction.
%
% EOP corrections not added when converting to J2000 according to Vallado
% 
[delPsi,trueEps,meanEps,Eq_e,~] = getNutationParams(TTT,iar80,rar80,2);
% Add EOP corrections to nutation parameters.
delPsi_corr = delPsi + dDPsi*AS2RAD;
trueEps_corr = trueEps + dDEps*AS2RAD;

%
% Polar motion. Vallado 4th Ed. Eq 3-77 (full) & Eq 3-78 (simplified)
%
c_x = cos(xp*AS2RAD);
s_x = sin(xp*AS2RAD);
c_y = cos(yp*AS2RAD);
s_y = sin(yp*AS2RAD);

% Complete polar motion matrix
WW = [  c_x      0      -s_x;
      s_x*s_y   c_y  c_x*s_y;
      s_x*c_y  -s_y  c_x*c_y];
% % Simplified with small angle assumption
% WW = [    1           0      -xp*AS2RAD;
%           0           1       yp*AS2RAD;
%        xp*AS2RAD  -yp*AS2RAD      1];

%
% Get seconds in UT1 & find days since Jan 1, 0h. (Fractional days - 1)
% Find Greenwich apparent sidereal time. [radians]
%
sec_ut1 = sec + dut1;
elapsed_days = finddays(yr,mo,day,hr,min,sec_ut1) - 1;
% Convert to radians (days->sec->deg-rad)
ed_rad = elapsed_days*(24*3600)/240*DEG2RAD;

gst0 = gstime0(yr);
gmst = gst0 + mod(1.002737909350795*ed_rad, 2*pi);
gast = gmst + Eq_e;

%
% Velocity correction term, Vallado 4th Ed. Eq 3-75
%
om_earth = [0; 0; 7.292115146706979e-5*(1 - lod/86400)];

%
% Get precession angles. Vallado 4th Ed. Eq 3-88
%
ze = (2306.2181*TTT + 0.30188*TTT^2 + 0.017998*TTT^3) * AS2RAD;
th = (2004.3109*TTT - 0.42665*TTT^2 - 0.041833*TTT^3) * AS2RAD;
z  = (2306.2181*TTT + 1.09468*TTT^2 + 0.018203*TTT^3) * AS2RAD;

% 
% Get rotation from PEF to GCRF frame
%
R_pef2tod = rot3mat(-gast);
R_tod2mod = rot1mat(-meanEps)*rot3mat(delPsi_corr)*rot1mat(trueEps_corr);
R_mod2eci = rot3mat(ze)*rot2mat(-th)*rot3mat(z);
PNR       = R_mod2eci*R_tod2mod*R_pef2tod;

%
% Perform transformation to GCRF frame.
%
r_gcrf = PNR*WW*r_ecef;
v_gcrf = PNR*(WW*v_ecef + cross(om_earth, WW*r_ecef));
a_gcrf = PNR*(WW*a_ecef + cross(om_earth,cross(om_earth,r_ecef)) ...
         + 2*cross(om_earth,v_ecef));

end