function [rotmat] = rot3mat(xval)
%%% Perform a third axis rotation.
%
% Author: Dylan Thomas
% Date: Feb 2, 2018
% Copyright: TBD

c = cos(xval);
s = sin(xval);

rotmat = [c s 0; -s c 0; 0 0 1];
end


