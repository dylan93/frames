function compare_vectors(r1,v1,r2,v2)
% Compare two vectors by via absolute & relative difference
%
% The first two vectors are the 'true' position and velocities, which are
% used as reference for the relative differences.
%
%
% Author: Dylan Thomas
% Date: Feb 2, 2018
% Copyright: TBD

rabs_diff = abs(r1-r2);
rrel_diff = rabs_diff./r1;
vabs_diff = abs(v1-v2);
vrel_diff = vabs_diff./v1;

fprintf('Position error:\n')
fprintf('Abs) % 7.5d % 7.5d % 7.5de\n',rabs_diff)
fprintf('Rel) % 7.5d % 7.5d % 7.5de\n\n',rrel_diff)
fprintf('Velocity error:\n')
fprintf('Abs) % 7.5d % 7.5d % 7.5de\n',vabs_diff)
fprintf('Rel) % 7.5d % 7.5d % 7.5de\n\n',vrel_diff)

end