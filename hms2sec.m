function [t_sec] = hms2sec(hr,min,t_sec)
% Convert time from hms form to seconds from beginning of the day.
%
% This is equivalent to the function in:
%
%   Fundamentals of Astrodynamics & Applications, David Vallado, Fourth Edition, 2013.
%
%   http://celestrak.com/software/vallado-sw.asp
%
%
% Author: Dylan Thomas
% January 25, 2018
% Copyright: TBD
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Code functionality
%
% Inputs:
%       
%       hr      - [hours]   - Hours in the current day
%       min     - [minutes] - Minutes in the current hour
%       sec     - [seconds] - Seconds in the current minute
%
%
% Outputs:
%
%       t_sec   - [sec]      - Seconds since the beginning of the day
%
%
% Dependencies:
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

t_sec  = hr * 3600 + min * 60 + t_sec;

